package com.ivanovrb.petprojectapplication

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = AdapterViewPager2()
        view_pager.adapter = adapter

        text_view.setOnClickListener { view_pager.requestLayout() }

        view_pager.setPageTransformer(HeightPageTransformer())
        view_pager.requestTransform()
        view_pager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        view_pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

            override fun onPageSelected(position: Int) {
                Log.d(MainActivity::class.java.simpleName, "Selected $position")
                view_pager.requestTransform()
            }
        })

        val position = intent.getIntExtra("position", 0).orDefault(0)
        view_pager.currentItem = position
    }

}

inline fun <reified T> T.logd(message: String) {
    Log.d(T::class.java.simpleName, message)
}
