package com.ivanovrb.petprojectapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import kotlinx.android.synthetic.main.item_pager.view.*


class ViewPagerAdapter : PagerAdapter() {

    val items = listOf(
        State(true, true, true),
        State(true, false, true),
        State(true, false, false)
    )

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(container.context).inflate(R.layout.item_pager, container, false)
        val state = items[position]
        with(itemView) {
            iv1.visibile = state.first
            iv2.visibile = state.second
            iv3.visibile = state.third
        }
        container.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as? View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return items.size
    }
}