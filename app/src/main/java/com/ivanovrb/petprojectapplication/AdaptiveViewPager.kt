package com.ivanovrb.petprojectapplication

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.core.view.children
import androidx.viewpager.widget.ViewPager
import kotlin.math.abs


class AdaptiveViewPager(context: Context, attrs: AttributeSet?) : ViewPager(context, attrs) {

    private val windowWidth = context.resources.displayMetrics.widthPixels

    private var isEdit = false

    private var oldPosition: Float = 0f

    private val viewsHeight = hashMapOf<Int, Int>()

    init {
        if (isInEditMode) {
            measure(measuredWidth, 0)
        }

        var nextView: Int? = null
        setPageTransformer(false, { view, position ->
            isEdit = true
            val pageWidth = view.width

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.alpha = 1f
                nextView = viewsHeight[currentItem.dec()]

            } else if (position <= 1) { // [-1,1]
                val currentHeight = viewsHeight[currentItem] ?: 0
                if (position > 0) {
                    val nextHeight = viewsHeight[currentItem.inc()] ?: 0
                    if (nextView != null) {
                        layoutParams = layoutParams.apply {
                            height = (currentHeight + (currentHeight - nextView!!) * position).toInt()
                        }
                    }
                } else {
                    val previousHeight = viewsHeight[currentItem.dec()] ?: 0
                    if (nextView !=null) {
                        layoutParams = layoutParams.apply {
                            height = (currentHeight + (currentHeight - nextView!!) * position).toInt()
                        }
                    }
                }
            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.alpha = 1f
                nextView = viewsHeight[currentItem.inc()]
            }
        }, View.LAYER_TYPE_NONE)

        addOnPageChangeListener(object : SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                isEdit = false
            }
        })
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var newHeightMeasureSpec = heightMeasureSpec

        children.forEach { child ->
            child.measure(child.measuredWidth, child.measuredHeight)
            viewsHeight[currentItem] = child.measuredHeight
        }
        if (!isEdit) {
            val viewedView: View? = findViewedChild()
            viewedView?.let {
                viewsHeight[currentItem] = viewedView.measuredHeight
                findNextChild()?.let { viewsHeight[currentItem + 1] = it.measuredHeight }
                newHeightMeasureSpec =
                    MeasureSpec.makeMeasureSpec(viewedView.measuredHeight, MeasureSpec.EXACTLY)
            }
        }
        super.onMeasure(widthMeasureSpec, newHeightMeasureSpec)
    }

    private fun findViewedChild(): View? {
        return children.filter { child ->
            val xPoint = child.left
            xPoint == currentItem * windowWidth
        }.firstOrNull()
    }

    private fun findNextChild(): View? {
        return children.filter { child ->
            val xPoint = child.left
            xPoint == (currentItem + 1) * windowWidth
        }.firstOrNull()
    }
}