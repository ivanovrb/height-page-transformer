package com.ivanovrb.petprojectapplication

import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.core.view.updateLayoutParams
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import kotlin.math.abs


class HeightPageTransformer : ViewPager2.PageTransformer, ViewPager.PageTransformer {

    var isLeft = false

    var primaryHeight = -1

    var previousPosition = 1f

    override fun transformPage(page: View, position: Float) {
        logd("${page.height} $position")


        logd("$isLeft $position $previousPosition")
        when {
            isLeft && position < 0 && abs(position - previousPosition) > 0.8 -> {
                reset()
            }
            !isLeft && position > 0 && abs(previousPosition - position) > 0.8 -> {
                reset()
            }
        }

        when {
            position < -0.99 -> {
//                reset()
            }
            position > 0.99 -> {
//                reset()
            }
            position > -1 && position < 0 -> {
                if (primaryHeight == -1 && position <= -0.5) {
                    logd("Left")
                    init(true, page)
                }

                if (isLeft) {
                    logd("Measure left")
                    measure(page, position)
                }

            }
            position == 0f -> {
                measure(page, position)
                reset()
            }
            position > 0 && position < 1 -> {
                if (primaryHeight == -1 && position >= 0.5) {
                    logd("Right")
                    init(false, page)
                }

                if (!isLeft) {
                    logd("Measure right")
                    measure(page, position)
                }
            }
        }
    }

    private fun init(isLeft: Boolean, page: View) {
        this.isLeft = isLeft
        primaryHeight =
            page.parent.asGeneric<ViewGroup>()?.measuredHeight.orDefault(primaryHeight)
    }

    private fun reset() {
        logd("RESET")
        previousPosition = 1f
        primaryHeight = -1
    }

    private fun measure(page: View, position: Float) {
        previousPosition = position

        if (primaryHeight == -1) {
            return
        }
        val childView = (page as? ViewGroup)?.children?.firstOrNull() ?: return
        childView.apply {
            measure(
                measuredWidth,
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
            )
        }
        val diff =
            ((childView.measuredHeight - primaryHeight) * (1 - abs(position))).toInt()

        page.parent.asGeneric<ViewGroup>()
            ?.updateLayoutParams<ViewGroup.LayoutParams> { height = primaryHeight + diff }

    }
}

inline fun <reified T> Any?.asGeneric(): T? = this as? T

fun <T> T?.orDefault(t: T): T = this ?: t