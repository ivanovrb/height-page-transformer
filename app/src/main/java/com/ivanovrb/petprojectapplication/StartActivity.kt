package com.ivanovrb.petprojectapplication

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_start.*

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)


        items.adapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_list_item_1,
            (0 until 9).map { "Item $it" })

        items.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                startActivity(Intent(this@StartActivity, MainActivity::class.java).apply {
                    putExtra(
                        "position",
                        position
                    )
                })}
        }
//        items.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//            }
//
//            override fun onItemSelected(
//                parent: AdapterView<*>?,
//                view: View?,
//                position: Int,
//                id: Long
//            ) {
//                startActivity(Intent(this@StartActivity, MainActivity::class.java).apply {
//                    putExtra(
//                        "position",
//                        position
//                    )
//                })
//            }
//        }
    }
}
