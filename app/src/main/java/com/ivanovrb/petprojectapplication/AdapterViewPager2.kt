package com.ivanovrb.petprojectapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_pager.view.*


class AdapterViewPager2 : RecyclerView.Adapter<AdapterViewPager2.ViewHolder>() {

    val items = listOf(
        State(true, true, true),
        State(true, false, true),
        State(true, false, false),
        State(false, false, true),
        State(true, true, false),
        State(true, false, true),
        State(false, true, false),
        State(true, false, false),
        State(true, true, false),
        State(false, true, true),
        State(false, false, true)
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_pager))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(state: State) = with(itemView) {
            iv1.visibile = state.first
            iv2.visibile = state.second
            iv3.visibile = state.third
        }
    }
}

data class State(
    val first: Boolean,
    val second: Boolean,
    val third: Boolean
)

fun ViewGroup.inflate(@LayoutRes layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

var View.visibile
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = View.VISIBLE.takeIf { value } ?: View.GONE
    }